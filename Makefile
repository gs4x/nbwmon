UNAME := $(shell uname)

ifeq ($(UNAME), DragonFly)
CFLAGS += -march=native -std=c11 -O2 -pedantic -Wall -Wextra -Wshadow -Wstrict-overflow -fno-strict-aliasing -I/usr/local/include -L/usr/local/lib
else ifeq ($(UNAME), NetBSD)
CFLAGS += -march=native -std=c11 -O2 -pedantic -Wall -Wextra -Wshadow -Wstrict-overflow -fno-strict-aliasing -I/usr/pkg/include -L/usr/pkg/lib
else
CFLAGS += -march=native -std=c18 -O2 -pedantic -Wall -Wextra -Wshadow -Wstrict-overflow -fno-strict-aliasing
endif

LDLIBS = -lncurses
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

TARGET_EXEC ?= nbwmon

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src
DOC_DIRS ?= ./doc

SRCS := $(shell find $(SRC_DIRS) -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS) $(LDLIBS)

$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@ $(LDLIBS)


.PHONY: clean install uninstall

clean:
	$(RM) -r $(BUILD_DIR)

install:
	$(MKDIR_P) $(DESTDIR)$(PREFIX)/bin
	$(CP_F) $(BUILD_DIR)/$(TARGET_EXEC) $(DESTDIR)$(PREFIX)/bin/
	$(MKDIR_P) $(DESTDIR)$(MANPREFIX)/man1
	$(CP_F) $(DOC_DIRS)/$(TARGET_EXEC).1 $(DESTDIR)$(MANPREFIX)/man1

uninstall:
	$(RM) $(DESTDIR)$(PREFIX)/bin/$(TARGET_EXEC)
	$(RM) $(DESTDIR)$(MANPREFIX)/man1/$(TARGET_EXEC).1

-include $(DEPS)

MKDIR_P ?= mkdir -p
CP_F ?= cp -f
