// build with
// zig build --build-file build-aarch64.zig -Dtarget=aarch64-linux-gnu
// for raspberry pi 3+ with 64bit linux
// ncurses header and libraries from the target architecture are needed

const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const flags = [_][]const u8{
        "-std=c18",
        "-pedantic",
        "-Wall",
        "-Wextra",
        "-Wshadow",
        "-Wstrict-overflow",
        "-fno-strict-aliasing",
    };

    const exe = b.addExecutable("nbwmon", null);

    const target = b.standardTargetOptions(.{});
    exe.setTarget(target);

    const mode = b.standardReleaseOptions();
    exe.setBuildMode(mode);

    exe.addCSourceFile("src/nbwmon.c", &flags);
    exe.addCSourceFile("src/util.c", &flags);

    exe.linkLibC();
    exe.addIncludeDir("/home/gsx/dev/aarch64/ncurses/include");
    exe.addLibPath("/home/gsx/dev/aarch64/ncurses/lib");
    exe.linkSystemLibraryName("ncurses");

    exe.install();
}
