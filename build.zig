const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const flags = [_][]const u8{
        "-std=c18",
        "-pedantic",
        "-Wall",
        "-Wextra",
        "-Wshadow",
        "-Wstrict-overflow",
        "-fno-strict-aliasing",
    };

    const exe = b.addExecutable("nbwmon", null);

    const target = b.standardTargetOptions(.{});
    exe.setTarget(target);

    const mode = b.standardReleaseOptions();
    exe.setBuildMode(mode);

    exe.addCSourceFile("src/nbwmon.c", &flags);
    exe.addCSourceFile("src/util.c", &flags);

    exe.linkLibC();
    exe.linkSystemLibrary("ncurses");
    // exe.addIncludeDir("/usr/include");
    // exe.addLibPath("/usr/lib");
    // exe.linkSystemLibraryName("ncurses");

    exe.install();
}
